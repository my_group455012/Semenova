let currentTurn = 'x';

/**
 * Передача хода
 */
function changeTurn () {
    const makeTurn = document.querySelector('.turn');

    if(currentTurn === 'x') {
        currentTurn = 'o';
    } else {
        currentTurn = 'x';
    }
    makeTurn.className = `turn cell-${currentTurn}`;
}
function onCellClick (event) {
    const elem = event.target;
    const elemClasses = elem.className.split(' ');
    if (elemClasses.indexOf('cell-x') === -1 && elemClasses.indexOf('cell-o') === -1) {
        elem.classList.add(`cell-${currentTurn}`);
        changeTurn();
    }
}

document.querySelectorAll('.cell').forEach((element) => {
    element.addEventListener('click', onCellClick);
});



