
const gameField = [
    ['o', 'o', 'o'],
    ['x', null, null],
    ['x', 'x', 'x']
];


function checkWin(field) {
    if (checkVertical(0,field)) {
        return getWinner(field[0][0]);
    } else if (checkVertical(1,field)) {
        return getWinner(field[0][1]);
    } else if (checkVertical(2,field)) {
        return getWinner(field[0][2]);
    } else if (checkHorizontal(0,field)) {
        return getWinner(field[0][0]);
    } else if (checkHorizontal(1,field)) {
        return getWinner(field[1][0]);
    } else if (checkHorizontal(2,field)) {
        return getWinner(field[2][0]);
    } else if (checkDiagonal(field)) {
        return getWinner(field[1][1]);
    }
    else {
        return 'Ничья';
    }
}

function getWinner(winner) {
    if (winner === 'x') {
        return 'Крестики победили';
    } else if (winner === 'o') {
        return  'Нолики победили';
    } else {
        return 'Странный победитель';
    }
}

function checkVertical(number,field) {
    return field[0][number] === field[1][number] &&
            field[0][number] === field[2][number] &&
            field[0][number] !== null;
}

function checkHorizontal(number,field) {
    return field[number][0]=== field[number][1] &&
            field[number][0] === field[number][2] &&
            field[number][0] !== null;
}

function checkDiagonal(field) {
    return (field[0][0] === field[1][1] &&
            field[0][0] === field[2][2] &&
            field[0][0] !== null) ||
        (field[2][0] === field[1][1] &&
            field[2][0] === field[0][2] &&
            field[2][0] !== null);
}

const result = checkWin(gameField);

document.write(result);
