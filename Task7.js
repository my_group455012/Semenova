const usernameInput = document.getElementById('username');

const userpassInput = document.getElementById('userpass');

const loginButton = document.getElementById('login');

function pressButton() {
    const username = usernameInput.value;
    const password = userpassInput.value;
    console.log(`Login: ${username}, password: ${password}`);
}
  loginButton.addEventListener("click", pressButton);

document.querySelector('form').addEventListener("submit", function () {
    return false;
})

function checkName(evt) {
    const charCode = evt.charCode;
    if (charCode != 0) {
        if (
            (charCode < 48 || charCode > 57) &&
            (charCode < 65 || charCode > 90) &&
            (charCode < 97 || charCode > 122) &&
            charCode !== 95 &&
            charCode !== 46
        ) {
            evt.preventDefault();
            alert(
                "Пожалуйста, используйте только буквы на латинице, цифры, _ или ."
                + "\n" + "charCode: " + charCode + "\n"
            );
        }
    }
}

usernameInput.addEventListener( 'keypress', checkName);
userpassInput.addEventListener( 'keypress', checkName);

function rejectCopy(event) {
    event.preventDefault();
    alert('Копирование запрещено');
}

usernameInput.addEventListener( 'copy', rejectCopy);
userpassInput.addEventListener( 'copy', rejectCopy);

usernameInput.addEventListener( 'cut', rejectCopy);
userpassInput.addEventListener( 'cut', rejectCopy);



