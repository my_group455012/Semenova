class GameField {
    constructor() {
        this.state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
            ];
        this.mode = 'x';
        this.isOverGame = false;
        this.winner = 'nobody';
    }

    /**
     * Меняем ход
     */
    setMode() {
        if(this.mode === 'x') {
            this.mode = 'o';
        } else {
            this.mode = 'x';
        }
    }

    /**
     * Получаем состояние хода
     */
    getGameFieldStatus() {
        return this.state;
    }

    /**
     * Делаем ход
     */

    makeTurn(vert,horiz) {
        if (this.isValidCoord(vert) && this.isValidCoord(horiz) && this.state[vert][horiz] === null) {
            const value = this.mode;
            this.state[vert][horiz] = value;

            if(this.hasWinner()) {
                this.winner = this.mode;
                this.isOverGame = true;
            }

            if(this.isFilledField()) {
                this.isOverGame = true;
            }

            if(!this.isOverGame) {
                this.setMode();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка заполненности всех клеток поля
     */

    isFilledField(){
        if(
            this.state[0][0] !== null &&
            this.state[0][1] !== null &&
            this.state[0][2] !== null &&
            this.state[1][0] !== null &&
            this.state[1][1] !== null &&
            this.state[1][2] !== null &&
            this.state[2][0] !== null &&
            this.state[2][1] !== null &&
            this.state[2][2] !== null
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка победы
     */

    hasWinner () {
        if (
            this.checkVertical(0) ||
            this.checkVertical(1) ||
            this.checkVertical(2) ||
            this.checkHorizontal(0) ||
            this.checkHorizontal(1) ||
            this.checkHorizontal(2) ||
            this.checkDiagonal()
        ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Проверка правильности введенных координат
     */

    isValidCoord (coord) {
        if(coord <= 2 && coord >= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Методы для проверки победы по горизонтали, вертикали и диагонали
     */

    checkVertical(number) {
    return this.state[0][number] === this.state[1][number] &&
           this.state[0][number] === this.state[2][number] &&
           this.state[0][number] !== null;
    }

    checkHorizontal(number) {
        return this.state[number][0] === this.state[number][1] &&
               this.state[number][0] === this.state[number][2] &&
               this.state[number][0] !== null;
    }

    checkDiagonal() {
        return (this.state[0][0] === this.state[1][1] &&
                this.state[0][0] === this.state[2][2] &&
                this.state[0][0] !== null) ||
                (this.state[2][0] === this.state[1][1] &&
                this.state[2][0] === this.state[0][2] &&
                this.state[2][0] !== null);
    }

    /**
    * Определяем кто победил
     */

    getWinner() {
        if(this.isOverGame) {
            if(this.winner === 'nobody') {
                return "Ничья";
            } else if (this.winner === 'x') {
                return "Победили крестики";
            } else if ( this.winner === 'o') {
                return "Победили нолики";
            } else {
                return "Победитель не определен";
            }
        } else {
            return "Игра еще идет";
        }
    }

    /**
     * Запуск игры
     */
    run() {
        let coord;
        while (!this.isOverGame) {
            coord = prompt('Введите координыаты хода через запятую, ходят ' + this.mode,'');
            coord = coord.split(',');
            this.makeTurn(coord[0], coord[1]);
        }
    }
}

let game = new GameField();
game.run();
document.write(game.state[0]);
document.write('<br />');
document.write(game.state[1]);
document.write('<br />');
document.write(game.state[2]);
document.write('<br />');
document.write(game.getWinner());

