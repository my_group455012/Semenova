const express = require('express');
const bodyParser = require('body-parser');
const { createHmac } = require('crypto');
const fs = require('fs');

const app = express();

const port = 3000;

app.set('view engine','ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));
app.use('/public', express.static('public'));

app.get('/authorization', function(req,res) {
    res.render('pages/authorization');
})

app.get('/rating', function(req,res) {

    let gamers = [
        {
             fio: 'Александров Игнат Анатолиевич',
             total: 24534,
             win: 9824,
             lose: 1222,
             percent: 87
        },{
             fio: 'Шевченко Рафаил Михайлович',
             total: 24534,
             win: 9824,
             lose: 1222,
             percent: 87
        },
    ];
    res.render('pages/rating', {
        gamers: gamers
    });
})

app.post('/login', function(req,res) {
    let allowedLogin = 'test';
    let login = req.body.username;
    let pass = req.body.userpass;

    fs.readFile('.secret', 'utf8', (err, hash) => {

        const passwordHash = createHmac('sha256', pass)
            .update('sa1t')
            .digest('hex');

        if (allowedLogin === login && passwordHash === hash) {
            res.status(301).redirect("http://localhost:8080/rating");
        } else {
            res.sendStatus(401);
        }
    })


})

app.listen(8080);

console.log('8080 is the magic port');
